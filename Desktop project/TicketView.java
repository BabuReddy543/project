package BusReservation;

import java.awt.Container;
import java.awt.FlowLayout;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class TicketView extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	@SuppressWarnings("unused")
	private Container contentPane;
	@SuppressWarnings("unused")
	private String str;
	int T_NO;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		TicketView frame = new TicketView();
		frame.setTitle("View Tickets Details");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public TicketView() {
		
		cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
		model.addColumn("T_NO");
		model.addColumn("C_ID");
		model.addColumn("SEAT_NO_FROM");
		model.addColumn("SEAT_NO_TO");
		model.addColumn("TOT_SEATS");
		model.addColumn("DOJ");
		model.addColumn("BUS_ID");

		try {
			System.out.println("in search");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			java.sql.Statement st = cn.createStatement();
			String query = "select * from tickets order by T_NO";
			// where T_NO= +T_NO //like '%"+str+"%'"
			System.out.println(query);
			ResultSet Rs = st.executeQuery(query);
			while (Rs.next() == true) {
				System.out.println(Rs.getString(1));
				// model.setRowCount(0);
				model.addRow(new Object[] { Rs.getInt(1), Rs.getInt(2), Rs.getInt(3), Rs.getInt(4), Rs.getInt(5),
						Rs.getDate(6), Rs.getInt(7) });

			}

		} catch (Exception e) {

		}
		JScrollPane pg = new JScrollPane(jtbl);
		cnt.add(pg);
		this.pack();

	}
}
