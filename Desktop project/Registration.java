package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class Registration extends JFrame {

	private JPanel contentPane;
	private JTextField txtfstname;
	private JTextField txtlastname;
	private JTextField txtmobile;
	private JTextField txtemail;
	private JTextField txtcreatepswd;
	private JTextField txtconformpswd;
	String FIRST_NAME, LAST_NAME, COUNTRY, STATE, MOB_NO, E_MAIL, CREATE_PSWD, CONFORM_PSWD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		Registration frame = new Registration();
		frame.setTitle("Registration");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Registration() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 547, 395);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRegistration = new JLabel("Registration");
		lblRegistration.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblRegistration.setBounds(195, 11, 120, 23);
		contentPane.add(lblRegistration);

		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblFirstName.setBounds(20, 85, 76, 14);
		contentPane.add(lblFirstName);

		txtfstname = new JTextField();
		txtfstname.setBounds(135, 82, 108, 20);
		contentPane.add(txtfstname);
		txtfstname.setColumns(10);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblLastName.setBounds(278, 85, 76, 14);
		contentPane.add(lblLastName);

		txtlastname = new JTextField();
		txtlastname.setBounds(375, 82, 108, 20);
		contentPane.add(txtlastname);
		txtlastname.setColumns(10);

		JLabel lblMobileNumber = new JLabel("Mobile No");
		lblMobileNumber.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblMobileNumber.setBounds(20, 173, 102, 14);
		contentPane.add(lblMobileNumber);

		txtmobile = new JTextField();
		txtmobile.setBounds(135, 170, 120, 20);
		contentPane.add(txtmobile);
		txtmobile.setColumns(10);

		JLabel lblEmail = new JLabel("E-mail ID");
		lblEmail.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblEmail.setBounds(278, 173, 67, 14);
		contentPane.add(lblEmail);

		txtemail = new JTextField();
		txtemail.setBounds(375, 170, 108, 20);
		contentPane.add(txtemail);
		txtemail.setColumns(10);

		JLabel lblCountry = new JLabel("Country");
		lblCountry.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblCountry.setBounds(20, 130, 67, 14);
		contentPane.add(lblCountry);

		String country[] = { "None", "India", "China", "Japan", "Canada" };
		JComboBox cmbcountry = new JComboBox(country);
		cmbcountry.setBounds(135, 127, 108, 20);
		contentPane.add(cmbcountry);

		JLabel lblState = new JLabel("State");
		lblState.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblState.setBounds(278, 130, 46, 14);
		contentPane.add(lblState);

		String state[] = { "None", "Andhra Pradesh", "karnataka", "Tamil Nadu", "Madhya Pradesh", "Telangana" };
		JComboBox cmbstate = new JComboBox(state);
		cmbstate.setBounds(375, 127, 108, 20);
		contentPane.add(cmbstate);

		JLabel lblCreatePassword = new JLabel("Create Password");
		lblCreatePassword.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblCreatePassword.setBounds(20, 219, 119, 14);
		contentPane.add(lblCreatePassword);

		txtcreatepswd = new JTextField();
		txtcreatepswd.setBounds(172, 216, 127, 20);
		contentPane.add(txtcreatepswd);
		txtcreatepswd.setColumns(10);

		JLabel lblConformPassword = new JLabel("Conform Password");
		lblConformPassword.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblConformPassword.setBounds(20, 258, 119, 14);
		contentPane.add(lblConformPassword);

		txtconformpswd = new JTextField();
		txtconformpswd.setBounds(172, 255, 127, 20);
		contentPane.add(txtconformpswd);
		txtconformpswd.setColumns(10);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBackground(Color.GREEN);
		btnCancel.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnCancel.setBounds(351, 307, 102, 38);
		contentPane.add(btnCancel);

		JButton btnRegister = new JButton("Register");
		btnRegister.setBackground(Color.GREEN);
		btnRegister.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				FIRST_NAME = txtfstname.getText();
				LAST_NAME = txtlastname.getText();
				COUNTRY = cmbcountry.getSelectedItem().toString();
				STATE = cmbstate.getSelectedItem().toString();
				MOB_NO = txtmobile.getText();
				E_MAIL = txtemail.getText();
				CREATE_PSWD = txtcreatepswd.getText();
				CONFORM_PSWD = txtconformpswd.getText();

				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					PreparedStatement st = cn
							.prepareStatement("insert into Registration values (RegisterSeq.nextval,?,?,?,?,?,?,?,?)");
					st.setString(1, FIRST_NAME);
					st.setString(2, LAST_NAME);
					st.setString(3, COUNTRY);
					st.setString(4, STATE);
					st.setString(5, MOB_NO);
					st.setString(6, E_MAIL);
					st.setString(7, CREATE_PSWD);
					st.setString(8, CONFORM_PSWD);
					int i = st.executeUpdate();
					if (i != 0) {
						JOptionPane.showMessageDialog(null, "Registered sucessfully", "Welcome",
								JOptionPane.PLAIN_MESSAGE);
						LoginPage frame = new LoginPage();
						frame.setVisible(true);
                         show(false);
					} else
						JOptionPane.showMessageDialog(null, "Not Registered Successfully", "Error",
								JOptionPane.ERROR_MESSAGE);

				} catch (ClassNotFoundException c) {

				} catch (SQLException e1) {
					e1.printStackTrace();

				}

			}
		});
		btnRegister.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnRegister.setBounds(135, 307, 119, 38);
		contentPane.add(btnRegister);

	}
}
