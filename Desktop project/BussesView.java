package BusReservation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;
import java.sql.*;

@SuppressWarnings("serial")
public class BussesView extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	JTextField txtSear = new JTextField();

	public BussesView() {
		getContentPane().setBackground(new Color(102, 204, 255));
		cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
		model.addColumn("BUS_ID");
		model.addColumn("BUS_NO");
		model.addColumn("TKT_COST");
		model.addColumn("BUS_TYPE ");
		model.addColumn("SOURCE");
		model.addColumn("DESTINATION");
		model.addColumn("DEPARTURE_TIME");
		model.addColumn("ARRIVAL_TIME");
		model.addColumn("DISTANCE");

		JButton btnBack = new JButton("Back");
		btnBack.setBackground(Color.GREEN);
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AdminHome frame = new AdminHome();
				frame.setVisible(true);
				show(false);

			}
		});
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBack.setBounds(335, 194, 89, 22);
		cnt.add(btnBack);

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			PreparedStatement pstm = cn.prepareStatement("SELECT * from bus order by BUS_ID");
			ResultSet Rs = pstm.executeQuery();
			while (Rs.next()) {
				model.addRow(new Object[] { Rs.getInt(1), Rs.getString(2), Rs.getString(3), Rs.getString(4),
						Rs.getString(5), Rs.getString(6), Rs.getString(7), Rs.getString(8), Rs.getString(9) });
			}
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(Color.GREEN);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					System.out.println("in search");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str = txtSear.getText().trim();
					System.out.println(str);
					java.sql.Statement st = cn.createStatement();
					String query = "select * from bus where BUS_TYPE like '%" + str + "%'";
					System.out.println(query);
					ResultSet Rs = st.executeQuery(query);
					while (Rs.next() == true) {
						System.out.println(Rs.getString(3));
						model.setRowCount(0);
						model.addRow(new Object[] { Rs.getInt(1), Rs.getString(2), Rs.getString(3), Rs.getString(4),
								Rs.getString(5), Rs.getString(6), Rs.getString(7), Rs.getString(8), Rs.getString(9) });

					}

				} catch (Exception e) {

				}

			}
		});
		JLabel lblBusname = new JLabel("BusType");
		lblBusname.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		getContentPane().add(lblBusname);

		getContentPane().add(txtSear);
		txtSear.setColumns(10);
		btnSearch.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		getContentPane().add(btnSearch);

		JScrollPane pg = new JScrollPane(jtbl);
		cnt.add(pg);
		this.pack();
	}

	public static void main(String[] args) {
		JFrame fr = new BussesView();
		fr.setTitle("Bus View Details");
		fr.setSize(500, 500);
		fr.setLocationRelativeTo(null);
		fr.setVisible(true);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
