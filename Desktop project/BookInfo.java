package BusReservation;

import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Color;

@SuppressWarnings("serial")
public class BookInfo extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	String BUS_NO, TKT_COST, BUS_TYPE, SOURCE, DESTINATION, DEPARTURE_TIME, ARRIVAL_TIME, DISTANCE;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		BookInfo fr = new BookInfo();
		fr.setVisible(true);
		fr.setTitle("Book Info");
		fr.setSize(500, 500);
		fr.setLocationRelativeTo(null);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BookInfo() {
		getContentPane().setBackground(new Color(102, 204, 255));
		
		cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
		model.addColumn("BUS_ID");
		model.addColumn("BUS_NO");
		model.addColumn("TKT_COST");
		model.addColumn("BUS_TYPE ");
		model.addColumn("SOURCE");
		model.addColumn("DESTINATION");
		model.addColumn("DEPARTURE_TIME");
		model.addColumn("ARRIVAL_TIME");
		model.addColumn("DISTANCE");

		JLabel lblSource = new JLabel("Source");
		lblSource.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblSource.setBounds(27, 86, 46, 23);
		cnt.add(lblSource);

		JComboBox cmbsrc = new JComboBox();
		cmbsrc.setBackground(Color.WHITE);

		cmbsrc.setBounds(114, 87, 113, 20);
		cnt.add(cmbsrc);

		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblDestination.setBounds(277, 90, 77, 14);
		cnt.add(lblDestination);

		JComboBox cmbDest = new JComboBox();
		cmbDest.setBackground(Color.WHITE);

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from bus ";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbDest.addItem(rs.getString(6));
				cmbsrc.addItem(rs.getString(5));
			}
		} catch (Exception e) {

		}

		cmbDest.setBounds(374, 87, 113, 20);
		cnt.add(cmbDest);
		
				JLabel lblJourneyDate = new JLabel("Journey Date");
				lblJourneyDate.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
				lblJourneyDate.setBounds(22, 151, 94, 14);
				cnt.add(lblJourneyDate);

		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(114, 145, 113, 31);
		cnt.add(dateChooser);

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			PreparedStatement pstm = cn.prepareStatement("SELECT * from bus");
			ResultSet Rs = pstm.executeQuery();
			while (Rs.next()) {
				model.addRow(new Object[] { Rs.getInt(1), Rs.getString(2), Rs.getString(3), Rs.getString(4),
						Rs.getString(5), Rs.getString(6), Rs.getString(7), Rs.getString(8), Rs.getString(9) });
			}
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(Color.GREEN);
		btnSearch.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		btnSearch.setBounds(304, 147, 113, 29);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SOURCE = cmbsrc.getSelectedItem().toString();
				DESTINATION = cmbDest.getSelectedItem().toString();
				Search();
			}
		});
		cnt.add(btnSearch);

		
		JScrollPane pg = new JScrollPane(jtbl);
		cnt.add(pg);
		this.pack();

	}

	@SuppressWarnings("unused")
	public void Search() {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			java.sql.Statement st = cn.createStatement();
			String str = ("Select * from bus where source = '" + SOURCE + "'  And destination= '" + DESTINATION + "'");
			ResultSet rs = st.executeQuery(str);
			while (rs.next()) {
				System.out.println(rs.getString(5) + " " + rs.getString(6));
				model.setRowCount(0);
				model.addRow(new Object[] { rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9) });

			}

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
}
