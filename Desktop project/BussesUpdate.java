package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class BussesUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField txtarvlTime;
	private JTextField txtDeparTime;
	private JTextField txtDistan;
	private JTextField txtTkt;
	private JTextField txtbusno;
	private JTextField txtsrc;
	private JTextField txtbustype;
	private JTextField txtdest;
	int BUS_ID;
	String BUS_NO, TKT_COST, BUS_TYPE, SOURCE, DESTINATION, DEPARTURE_TIME, ARRIVAL_TIME, DISTANCE;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		BussesUpdate frame = new BussesUpdate();
		frame.setTitle("Update Busses");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BussesUpdate() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 564, 407);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblBusno = new JLabel("BusNo");
		lblBusno.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblBusno.setBounds(23, 116, 46, 25);
		contentPane.add(lblBusno);

		JLabel lblUpdateBusses = new JLabel("Update Busses");
		lblUpdateBusses.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblUpdateBusses.setBounds(212, 11, 154, 25);
		contentPane.add(lblUpdateBusses);

		txtbusno = new JTextField();
		txtbusno.setBounds(119, 118, 94, 20);
		contentPane.add(txtbusno);
		txtbusno.setColumns(10);

		JLabel lblBusId = new JLabel("Bus Id");
		lblBusId.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblBusId.setBounds(23, 81, 46, 14);
		contentPane.add(lblBusId);

		JLabel lblBustype = new JLabel("BusType");
		lblBustype.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblBustype.setBounds(279, 116, 62, 14);
		contentPane.add(lblBustype);

		JLabel lblSource = new JLabel("Source");
		lblSource.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblSource.setBounds(23, 153, 46, 25);
		contentPane.add(lblSource);

		JComboBox cmbbusid = new JComboBox();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from bus order by bus_id";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbbusid.addItem(rs.getString(1));

			}
		} catch (Exception e) {

		}
		cmbbusid.setBounds(119, 78, 94, 20);
		contentPane.add(cmbbusid);

		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDestination.setBounds(279, 158, 86, 14);
		contentPane.add(lblDestination);

		JLabel lblArrivaltime = new JLabel("ArrivalTime");
		lblArrivaltime.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblArrivaltime.setBounds(279, 193, 86, 25);
		contentPane.add(lblArrivaltime);

		txtarvlTime = new JTextField();
		txtarvlTime.setBounds(384, 195, 86, 20);
		contentPane.add(txtarvlTime);
		txtarvlTime.setColumns(10);

		JLabel lblDeparturetime = new JLabel("DepartureTime");
		lblDeparturetime.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDeparturetime.setBounds(10, 204, 99, 14);
		contentPane.add(lblDeparturetime);

		txtDeparTime = new JTextField();
		txtDeparTime.setBounds(119, 201, 94, 20);
		contentPane.add(txtDeparTime);
		txtDeparTime.setColumns(10);

		JLabel lblDistance = new JLabel("Distance");
		lblDistance.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDistance.setBounds(23, 232, 58, 25);
		contentPane.add(lblDistance);

		txtDistan = new JTextField();
		txtDistan.setBounds(115, 234, 98, 20);
		contentPane.add(txtDistan);
		txtDistan.setColumns(10);

		JLabel lblTktcost = new JLabel("TktCost");
		lblTktcost.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblTktcost.setBounds(279, 237, 62, 14);
		contentPane.add(lblTktcost);

		txtTkt = new JTextField();
		txtTkt.setBounds(384, 231, 86, 20);
		contentPane.add(txtTkt);
		txtTkt.setColumns(10);

		txtsrc = new JTextField();
		txtsrc.setBounds(115, 155, 98, 20);
		contentPane.add(txtsrc);
		txtsrc.setColumns(10);

		txtbustype = new JTextField();
		txtbustype.setBounds(384, 110, 86, 20);
		contentPane.add(txtbustype);
		txtbustype.setColumns(10);

		txtdest = new JTextField();
		txtdest.setBounds(384, 155, 86, 20);
		contentPane.add(txtdest);
		txtdest.setColumns(10);

		ItemListener itemListener = new ItemListener() {
			@SuppressWarnings("unused")
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				System.out.println("Bus: " + itemEvent.getItem()); // --
				String BUS_ID = (String) itemEvent.getItem();
				try {

					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str1 = " select BUS_NO,BUS_TYPE,SOURCE,DESTINATION,DEPARTURE_TIME,ARRIVAL_TIME,DISTANCE,TKT_COST from bus where bus_id = "
							+ BUS_ID;
					Statement st = cn.createStatement();
					System.out.println(str1);
					ResultSet rs = st.executeQuery(str1);

					while (rs.next()) {
						txtbusno.setText(rs.getString(1));
						txtbustype.setText(rs.getString(2));
						txtsrc.setText(rs.getString(3));
						txtdest.setText(rs.getString(4));
						txtDeparTime.setText(rs.getString(5));
						txtarvlTime.setText(rs.getString(6));
						txtDistan.setText(rs.getString(7));
						txtTkt.setText(rs.getString(8));
					}
				} catch (Exception e) {

				}
			}
		};
		cmbbusid.addItemListener(itemListener);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBackground(Color.GREEN);
		btnUpdate.addActionListener(new ActionListener() {
			@SuppressWarnings({ "unused", "deprecation" })
			public void actionPerformed(ActionEvent arg0) {

				Connection cn = null;
				PreparedStatement stmt = null;
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");

					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");

					PreparedStatement ps = cn.prepareStatement(
							"UPDATE bus SET BUS_NO=?,SOURCE=?,DESTINATION=?,ARRIVAL_TIME=?,DEPARTURE_TIME=?,DISTANCE=?,TKT_COST=? WHERE BUS_ID=?");

					ps.setString(1, txtbusno.getText());
					ps.setString(2, txtsrc.getText());
					ps.setString(3, txtdest.getText());
					ps.setString(4, txtarvlTime.getText());
					ps.setString(5, txtDeparTime.getText());
					ps.setString(6, txtDistan.getText());
					ps.setString(7, txtTkt.getText());
					ps.setString(8, (String) cmbbusid.getSelectedItem());

					int i = ps.executeUpdate();
					if (i != 0) {
						JOptionPane.showMessageDialog(null, "Bus updated sucessfully", "Welcome",
								JOptionPane.PLAIN_MESSAGE);
						show(false);
						AdminHome frame = new AdminHome();
						frame.setVisible(true);
						show(false);
					} else {
						JOptionPane.showMessageDialog(null, "Bus Not updated Successfully", "Error",
								JOptionPane.ERROR_MESSAGE);
						show(false);
						BussesUpdate frame = new BussesUpdate();
						frame.setVisible(true);
                        show(false);

					}
				} catch (ClassNotFoundException e) {

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnUpdate.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnUpdate.setBounds(126, 306, 109, 34);
		contentPane.add(btnUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				AdminHome frame = new AdminHome();
				frame.setVisible(true);
				show(false);

			}
		});
		btnBack.setBackground(Color.GREEN);
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBack.setBounds(329, 306, 89, 35);
		contentPane.add(btnBack);

	}

}
