package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Color;

@SuppressWarnings("serial")
public class UpdateCustomer extends JFrame {

	private JPanel contentPane;
	private JTextField txtcusname;
	private JTextField txtphnmbr;
	String C_NAME, PH_NO, C_ID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		UpdateCustomer frame = new UpdateCustomer();
		frame.setTitle("Update Customer");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public UpdateCustomer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 537, 357);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUpdateCustomer = new JLabel("Update Customer");
		lblUpdateCustomer.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblUpdateCustomer.setBounds(182, 11, 164, 14);
		contentPane.add(lblUpdateCustomer);

		JLabel lblCusId = new JLabel("Cus ID");
		lblCusId.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblCusId.setBounds(42, 67, 46, 27);
		contentPane.add(lblCusId);

		JLabel lblCusName = new JLabel("Cus Name");
		lblCusName.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblCusName.setBounds(42, 118, 73, 20);
		contentPane.add(lblCusName);

		txtcusname = new JTextField();
		txtcusname.setBounds(158, 118, 106, 20);
		contentPane.add(txtcusname);
		txtcusname.setColumns(10);

		JLabel lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblPhoneNumber.setBounds(42, 167, 106, 20);
		contentPane.add(lblPhoneNumber);

		txtphnmbr = new JTextField();
		txtphnmbr.setBounds(158, 167, 106, 20);
		contentPane.add(txtphnmbr);
		txtphnmbr.setColumns(10);

		JComboBox cmbcid = new JComboBox();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from customer order by c_id";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbcid.addItem(rs.getString(1));

			}
		} catch (Exception e) {

		}
		cmbcid.setBounds(158, 70, 106, 20);
		contentPane.add(cmbcid);

		ItemListener itemListener = new ItemListener() {
			@SuppressWarnings("unused")
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				String C_ID = (String) itemEvent.getItem();
				try {

					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str1 = " select C_NAME,PH_NO from customer where c_id = " + C_ID;
					Statement st = cn.createStatement();
					ResultSet rs = st.executeQuery(str1);

					while (rs.next()) {
						txtcusname.setText(rs.getString(1));
						txtphnmbr.setText(rs.getString(2));

					}
				} catch (Exception e) {

				}
			}
		};
		cmbcid.addItemListener(itemListener);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBackground(Color.GREEN);
		btnUpdate.addActionListener(new ActionListener() {
			@SuppressWarnings({ "deprecation", "unused" })
			public void actionPerformed(ActionEvent arg0) {
				Connection cn = null;
				PreparedStatement stmt = null;
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");

					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					PreparedStatement ps = cn.prepareStatement("UPDATE customer set C_NAME=?,PH_NO=? where C_ID=?");
					ps.setString(1, txtcusname.getText());
					ps.setString(2, txtphnmbr.getText());
					ps.setString(3, (String) cmbcid.getSelectedItem());
					int i = ps.executeUpdate();
					if (i != 0) {
						JOptionPane.showMessageDialog(null, "Customer Updated sucessfully", "Welcome",
								JOptionPane.PLAIN_MESSAGE);
						show(false);
						AgentHome frame = new AgentHome();
						frame.setVisible(true);
						show(false);
					} else {
						JOptionPane.showMessageDialog(null, "Customer Not Updated Successfully", "Error",
								JOptionPane.ERROR_MESSAGE);
						show(false);
						UpdateCustomer frame = new UpdateCustomer();
						frame.setVisible(true);
						show(false);
					}
				} catch (ClassNotFoundException e) {

				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		});
		btnUpdate.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnUpdate.setBounds(182, 238, 136, 42);
		contentPane.add(btnUpdate);

	}
}
