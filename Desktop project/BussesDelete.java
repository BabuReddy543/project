package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class BussesDelete<cmbEnter> extends JFrame {

	private JPanel contentPane;
	String BUS_NO;
	@SuppressWarnings("unused")
	private int BUS_ID;

	/**
	 * Launch the application.
	 */
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {

		BussesDelete frame = new BussesDelete();
		frame.setTitle("Delete Busses");
		frame.setVisible(true);
		
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BussesDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDeleteBus = new JLabel("Delete Busses");
		lblDeleteBus.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblDeleteBus.setBounds(149, 22, 146, 35);
		contentPane.add(lblDeleteBus);

		JLabel lblEnterBusNo = new JLabel("Enter Bus No");
		lblEnterBusNo.setFont(new Font("Sitka Subheading", Font.BOLD, 15));
		lblEnterBusNo.setBounds(41, 86, 135, 35);
		contentPane.add(lblEnterBusNo);

		JComboBox cmbBusNo = new JComboBox();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from bus order by bus_no";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbBusNo.addItem(rs.getString(2));

			}
		} catch (Exception e) {

		}
		cmbBusNo.setBounds(197, 90, 123, 26);
		contentPane.add(cmbBusNo);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBackground(Color.GREEN);
		btnCancel.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnCancel.setBounds(278, 185, 109, 35);
		contentPane.add(btnCancel);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBackground(Color.GREEN);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BUS_NO = cmbBusNo.getSelectedItem().toString();
			

				delete(BUS_NO);

			}
		});
		btnDelete.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnDelete.setBounds(53, 185, 109, 35);
		contentPane.add(btnDelete);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	public void delete(String BUS_NO) {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			stmt = cn.createStatement();
			boolean i = stmt.execute("DELETE from bus where BUS_NO = '" + BUS_NO + "'");
			if (i == false) {
				JOptionPane.showMessageDialog(null, "Bus Deleted sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
				show(false);
				AdminHome frame = new AdminHome();
				frame.setVisible(true);
				show(false);
			} else
			{
				JOptionPane.showMessageDialog(null, "Bus Not Deleted Successfully", "Error", JOptionPane.ERROR_MESSAGE);
			show(false);
			BussesDelete frame = new BussesDelete();
			frame.setVisible(true);
			show(false);
			}

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
