package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JList;
import java.awt.Color;

@SuppressWarnings("serial")
public class AdminHome extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		AdminHome frame = new AdminHome();
		frame.setTitle("Admin Home");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public AdminHome() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 545, 360);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnBus = new JMenu("Bus");
		mnBus.setBackground(new Color(192, 192, 192));
		mnBus.setFont(new Font("Sitka Subheading", Font.BOLD, 12));
		menuBar.add(mnBus);

		JMenuItem mntmAdd = new JMenuItem("Add");
		mntmAdd.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				BussesAdd ad = new BussesAdd();
				ad.setVisible(true);
				show(false);
			}
		});
		mnBus.add(mntmAdd);

		JMenuItem mntmUpdate = new JMenuItem("Update");
		mntmUpdate.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				BussesUpdate frame = new BussesUpdate();
				frame.setVisible(true);
				show(false);
			}
		});
		mnBus.add(mntmUpdate);

		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.addActionListener(new ActionListener() {
			@SuppressWarnings({ "rawtypes", "deprecation" })
			public void actionPerformed(ActionEvent e) {
				BussesDelete frame = new BussesDelete();
				frame.setVisible(true);
				show(false);
			}
		});
		mnBus.add(mntmDelete);

		JMenuItem mntmView = new JMenuItem("View");
		mntmView.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				JFrame fr = new BussesView();
				fr.setVisible(true);
				show(false);
			}
		});
		mnBus.add(mntmView);

		JMenu mnExit = new JMenu("Exit");
		mnExit.setBackground(new Color(192, 192, 192));
		menuBar.add(mnExit);

		JMenuItem mntmLogout = new JMenuItem("Admin Logout");
		mntmLogout.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AdminLogout frame = new AdminLogout();
				frame.setVisible(true);
				show(false);
			}
		});
		mnExit.add(mntmLogout);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		@SuppressWarnings("rawtypes")
		JList list = new JList();
		list.setBounds(44, 108, -12, -19);
		contentPane.add(list);
	}

	@SuppressWarnings("unused")
	private static void addPopup(Component component,final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());

			}
		});
	}
}