package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

@SuppressWarnings("serial")
public class AddCustomer extends JFrame {

	private JPanel contentPane;
	private JTextField txtcname;
	private JTextField txtphn;
	String C_NAME, PH_NO;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		AddCustomer frame = new AddCustomer();
		frame.setTitle("Add Customer");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public AddCustomer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 544, 354);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAdd = new JButton("Add");
		btnAdd.setBackground(Color.GREEN);
		btnAdd.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				C_NAME = txtcname.getText();
				PH_NO = txtphn.getText();
				insert();
			}
		});
		btnAdd.setBounds(101, 213, 89, 30);
		contentPane.add(btnAdd);

		JLabel lblCu = new JLabel("C_Name");
		lblCu.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		lblCu.setBounds(46, 77, 61, 30);
		contentPane.add(lblCu);

		txtcname = new JTextField();
		txtcname.setBounds(182, 83, 149, 20);
		contentPane.add(txtcname);
		txtcname.setColumns(10);

		txtphn = new JTextField();
		txtphn.setBounds(182, 138, 143, 20);
		contentPane.add(txtphn);
		txtphn.setColumns(10);

		JLabel lblPhno = new JLabel("Ph_No");
		lblPhno.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		lblPhno.setBounds(46, 137, 61, 20);
		contentPane.add(lblPhno);

		JButton btnBack = new JButton("Back");
		btnBack.setBackground(Color.GREEN);
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AgentHome ah = new AgentHome();
				ah.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		btnBack.setBounds(306, 213, 89, 30);
		contentPane.add(btnBack);

		JLabel lblAddCustomer = new JLabel("Add Customer");
		lblAddCustomer.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblAddCustomer.setBounds(182, 11, 128, 20);
		contentPane.add(lblAddCustomer);
	}

	@SuppressWarnings("deprecation")
	public void insert() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");

			PreparedStatement st = cn.prepareStatement("insert into customer values (customerSeq.nextval,?,?)");
			st.setString(1, C_NAME);
			st.setString(2, PH_NO);
			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Customer Added sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
                show(false);
                AgentHome frame = new AgentHome();
        		frame.setVisible(true);
                show(false);
			} else
			{
				JOptionPane.showMessageDialog(null, "Customer Not Added Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);
                show(false);
                AddCustomer frame = new AddCustomer();
          		frame.setVisible(true);
          		show(false);
			}
          		

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		txtcname.setText(" ");
		txtphn.setText(" ");
	}
}
