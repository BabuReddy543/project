package BusReservation;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AgentHome extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		AgentHome frame = new AgentHome();
		frame.setTitle("Agent Home");
		frame.setVisible(true);

	}

	/**
	 * 1) Setting labels' for New Menu item, Customer, Add, Delete, Update.
	 */
	public AgentHome() {
		getContentPane().setBackground(new Color(102, 204, 255));
		this.SetLayOut();
	}

	public void SetLayOut() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 580, 369);
		getContentPane().setLayout(null);

		JMenuItem menuItem = new JMenuItem("New menu item");
		menuItem.setBounds(-16, 40, 66, 1);
		getContentPane().add(menuItem);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnCustomer = new JMenu("Customer");
		menuBar.add(mnCustomer);

		JMenuItem mntmAdd = new JMenuItem("Add");
		mntmAdd.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AddCustomer frame = new AddCustomer();
				frame.setVisible(true);
                show(false);
			}
		});
		mnCustomer.add(mntmAdd);

		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				DeleteCustomer frame = new DeleteCustomer();
				frame.setVisible(true);
				show(false);	
			}
		});
		mnCustomer.add(mntmDelete);

		JMenuItem mntmUpdate = new JMenuItem("Update");
		mntmUpdate.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				UpdateCustomer frame = new UpdateCustomer();
				frame.setVisible(true);
				show(false);
			}
		});
		mnCustomer.add(mntmUpdate);
		
		JMenuItem mntmView_1 = new JMenuItem("View");
		mntmView_1.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				JFrame fr = new ViewCustomer();
				fr.setVisible(true);
				show(false);
			}
		});
		mnCustomer.add(mntmView_1);


		JMenu mnBus = new JMenu("Bus");
		menuBar.add(mnBus);
		
		JMenuItem mntmView = new JMenuItem("View");
		mntmView.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				JFrame frm = new BusView();
				frm.setVisible(true);
				show(false);	
			}
		});
		mnBus.add(mntmView);

		JMenu mnTicket = new JMenu("Ticket");
		menuBar.add(mnTicket);

		JMenu mnBooking = new JMenu("Booking");
		mnTicket.add(mnBooking);

		JMenuItem mntmBookInfo = new JMenuItem("Book Info");
		mntmBookInfo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				BookInfo fr = new BookInfo();
				fr.setVisible(true);
				show(false);
			}
		});
		mnBooking.add(mntmBookInfo);

		JMenuItem mntmTicketBook = new JMenuItem("Ticket Book");
		mntmTicketBook.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				Tickets frame = new Tickets();
				frame.setVisible(true);
				show(false);
			}
		});
		mnBooking.add(mntmTicketBook);

		JMenu mnExit = new JMenu("Exit");
		menuBar.add(mnExit);

		JMenuItem mntmLogout = new JMenuItem("Agent Logout");
		mntmLogout.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AgentLogout frame = new AgentLogout();
				frame.setVisible(true);
				show(false);
				
			}
		});
		mnExit.add(mntmLogout);
	}
}
