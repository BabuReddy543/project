package BusReservation;

import java.sql.Connection;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class LoginPage extends JFrame {

	private JPanel contentPane;
	private JTextField txtUserName;
	// public Component f1;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		LoginPage frame = new LoginPage();
		frame.setTitle("Login ");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		setBounds(100, 100, 536, 362);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("UserName :");
		lblUsername.setFont(new Font("Sitka Banner", Font.BOLD, 14));
		lblUsername.setBounds(60, 59, 118, 53);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Sitka Banner", Font.BOLD, 14));
		lblPassword.setBounds(60, 106, 94, 37);
		contentPane.add(lblPassword);

		txtUserName = new JTextField();
		txtUserName.setBackground(Color.WHITE);
		txtUserName.setBounds(180, 75, 174, 20);
		contentPane.add(txtUserName);
		txtUserName.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.setBackground(new Color(0, 255, 0));
		btnLogin.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		btnLogin.setBounds(92, 229, 89, 23);
		contentPane.add(btnLogin);

		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				LoginPage frame = new LoginPage();
				frame.setVisible(true);
				show(false);
			}
		});
		btnNewButton.setBackground(new Color(0, 255, 0));
		btnNewButton.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		btnNewButton.setBounds(265, 229, 89, 23);
		contentPane.add(btnNewButton);

		password = new JPasswordField();
		password.setToolTipText("");
		password.setEchoChar('*');
		password.setBounds(180, 114, 174, 20);
		contentPane.add(password);

		JLabel lblNewLabel = new JLabel("BABU TRAVELS");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Sitka Subheading", Font.BOLD, 25));
		lblNewLabel.setBounds(139, 11, 206, 37);
		contentPane.add(lblNewLabel);

		JRadioButton rdbtnAgent = new JRadioButton("Agent");
		rdbtnAgent.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		rdbtnAgent.setBackground(new Color(192, 192, 192));
		rdbtnAgent.setBounds(255, 189, 109, 23);
		contentPane.add(rdbtnAgent);

		JRadioButton rdbtnAdmin = new JRadioButton("Admin ");
		rdbtnAdmin.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		rdbtnAdmin.setBackground(new Color(192, 192, 192));
		rdbtnAdmin.setBounds(92, 189, 109, 23);
		contentPane.add(rdbtnAdmin);

		ButtonGroup b = new ButtonGroup();
		b.add(rdbtnAgent);
		b.add(rdbtnAdmin);

		JButton btnRegisterHere = new JButton("Register Here");
		btnRegisterHere.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				Registration frame = new Registration();
				frame.setVisible(true);
                show(false);
				
			}
		});
		btnRegisterHere.setBackground(Color.ORANGE);
		btnRegisterHere.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		btnRegisterHere.setBounds(217, 275, 147, 23);
		contentPane.add(btnRegisterHere);

		JLabel lblNewUser = new JLabel("New User ?");
		lblNewUser.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		lblNewUser.setBounds(112, 277, 89, 18);
		contentPane.add(lblNewUser);

		btnLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String Uname = txtUserName.getText();
				String Upass = password.getText();
				if (rdbtnAdmin.isSelected()) // admin
				{
					if (Uname.equals("admin") && Upass.equals("admin")) {

						JOptionPane.showMessageDialog(null, "Login Successful", "Welcome", JOptionPane.PLAIN_MESSAGE);
						AdminHome hm = new AdminHome();
						hm.setVisible(true);
						show(false);
					} else {
						JOptionPane.showMessageDialog(null, "Incorrect login or password", "Warning",
								JOptionPane.WARNING_MESSAGE);
					}

				}

				else if (rdbtnAgent.isSelected()) // agent
				{

					try {
						Class.forName("oracle.jdbc.driver.OracleDriver");
						Connection cn;
						cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
						java.sql.Statement st = cn.createStatement();
						String str = "select * from agent where user_name= '" + Uname + "' and password='" + Upass
								+ "'";
						ResultSet r = st.executeQuery(str);
						System.out.println(str);
						if (r.next()) {
							JOptionPane.showMessageDialog(null, "Login Successful", "Welcome",
									JOptionPane.PLAIN_MESSAGE);
							AgentHome ah = new AgentHome();
							ah.setVisible(true);
							show(false);
						} else {
							JOptionPane.showMessageDialog(null, "Incorrect login or password", "Warning",
									JOptionPane.WARNING_MESSAGE);
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		});
	}
}
