package BusReservation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

@SuppressWarnings("serial")
public class BussesAdd extends JFrame {

	private JPanel contentPane;
	private JTextField txtArrTime;
	private JTextField txtDepTime;
	private JTextField txtTktCost;
	private JTextField txtDistance;
	String BUS_NO, TKT_COST, BUS_TYPE, SOURCE, DESTINATION, DEPARTURE_TIME, ARRIVAL_TIME, DISTANCE;
	private JTextField txtbusno;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		BussesAdd frame = new BussesAdd();
		frame.setTitle("Add Busses");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BussesAdd() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 538, 420);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblBusno = new JLabel("Bus_No");
		lblBusno.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblBusno.setBounds(10, 78, 46, 14);
		contentPane.add(lblBusno);


		JLabel lblNewLabel = new JLabel("Bus _Type");
		lblNewLabel.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblNewLabel.setBounds(237, 78, 65, 14);
		contentPane.add(lblNewLabel);

		String bustype[] = { "None", "AC", "Non-AC", "Deluxe", "Luxury" };
		JComboBox cmbBusType = new JComboBox(bustype);
		cmbBusType.setBounds(343, 75, 86, 20);
		contentPane.add(cmbBusType);

		JLabel lblSource = new JLabel("Source");
		lblSource.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblSource.setBounds(10, 109, 46, 14);
		contentPane.add(lblSource);

		String source[] = { "Chennai", "Tirupati", "Bangalore", "Hyderabad" };
		JComboBox cmbSource = new JComboBox(source);
		cmbSource.setBounds(94, 106, 86, 20);
		contentPane.add(cmbSource);

		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDestination.setBounds(237, 109, 86, 14);
		contentPane.add(lblDestination);

		String Destination[] = { "None", "Tirupati", "Hyderabad", "Bangalore" };
		JComboBox cmbDestination = new JComboBox(Destination);
		cmbDestination.setBounds(343, 106, 86, 20);
		contentPane.add(cmbDestination);

		JLabel lblArrival = new JLabel("Arrival_Time");
		lblArrival.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblArrival.setBounds(45, 168, 95, 14);
		contentPane.add(lblArrival);

		JLabel lblDeparturetime = new JLabel("Departure_Time");
		lblDeparturetime.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDeparturetime.setBounds(45, 193, 135, 14);
		contentPane.add(lblDeparturetime);

		JLabel lblDistance = new JLabel("Distance");
		lblDistance.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDistance.setBounds(256, 233, 67, 14);
		contentPane.add(lblDistance);

		JLabel lblTktcost = new JLabel("Tkt_Cost");
		lblTktcost.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblTktcost.setBounds(25, 230, 65, 20);
		contentPane.add(lblTktcost);

		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnAdd.setBackground(Color.GREEN);
		btnAdd.setBounds(91, 310, 89, 33);
		contentPane.add(btnAdd);

		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				BUS_NO = txtbusno.getText();
				TKT_COST = txtTktCost.getText();
				BUS_TYPE = cmbBusType.getSelectedItem().toString();
				SOURCE = cmbSource.getSelectedItem().toString();
				DESTINATION = cmbDestination.getSelectedItem().toString();
				DEPARTURE_TIME = txtDepTime.getText();
				ARRIVAL_TIME = txtArrTime.getText();
				DISTANCE = txtDistance.getText();

				insert();
			}
		});

		txtArrTime = new JTextField();
		txtArrTime.setBounds(207, 156, 86, 20);
		contentPane.add(txtArrTime);
		txtArrTime.setColumns(10);

		txtDepTime = new JTextField();
		txtDepTime.setBounds(207, 190, 86, 20);
		contentPane.add(txtDepTime);
		txtDepTime.setColumns(10);

		txtTktCost = new JTextField();
		txtTktCost.setBounds(115, 230, 86, 20);
		contentPane.add(txtTktCost);
		txtTktCost.setColumns(10);

		txtDistance = new JTextField();
		txtDistance.setBounds(343, 230, 86, 20);
		contentPane.add(txtDistance);
		txtDistance.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Busses Add");
		lblNewLabel_1.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblNewLabel_1.setBounds(207, 11, 126, 26);
		contentPane.add(lblNewLabel_1);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AdminHome frame = new AdminHome();
				frame.setVisible(true);
				show(false);
			}
		});
		btnBack.setBackground(Color.GREEN);
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBack.setBounds(301, 310, 89, 33);
		contentPane.add(btnBack);
		
		txtbusno = new JTextField();
		txtbusno.setBounds(94, 75, 86, 20);
		contentPane.add(txtbusno);
		txtbusno.setColumns(10);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	public void insert() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");

			PreparedStatement st = cn.prepareStatement("insert into bus values (sequence_1.nextval,?,?,?,?,?,?,?,?)");
			st.setString(1, BUS_NO);
			st.setString(2, TKT_COST);
			st.setString(3, BUS_TYPE);
			st.setString(4, SOURCE);
			st.setString(5, DESTINATION);
			st.setString(6, DEPARTURE_TIME);
			st.setString(7, ARRIVAL_TIME);
			st.setString(8, DISTANCE);
			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Bus added sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
				show(false);
				AdminHome frame = new AdminHome();
				frame.setVisible(true);
				show(false);
			} else
			{
				JOptionPane.showMessageDialog(null, "Bus Not added Successfully", "Error", JOptionPane.ERROR_MESSAGE);
			//show(false);
			BussesAdd frame = new BussesAdd();
			frame.setVisible(true);
			show(false);
			}

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
