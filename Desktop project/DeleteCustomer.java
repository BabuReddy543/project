package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class DeleteCustomer extends JFrame {

	private JPanel contentPane;
	int C_ID, PH_NO;
	String C_NAME;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		DeleteCustomer frame = new DeleteCustomer();
		frame.setTitle("Delete Customer");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DeleteCustomer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 409);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEnterCustomerName = new JLabel("Enter Customer Name");
		lblEnterCustomerName.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblEnterCustomerName.setBounds(47, 123, 152, 27);
		contentPane.add(lblEnterCustomerName);
		
		JComboBox cmbcus = new JComboBox();
		cmbcus.setBounds(263, 126, 117, 20);
		contentPane.add(cmbcus);

		JLabel lblDeleteCustomer = new JLabel("Delete Customer");
		lblDeleteCustomer.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblDeleteCustomer.setBounds(187, 11, 152, 27);
		contentPane.add(lblDeleteCustomer);
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from customer order by c_id";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbcus.addItem(rs.getString(1) + "  " + rs.getString(2));

			}
		} catch (Exception e) {

		}

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBackground(Color.GREEN);
		btnCancel.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnCancel.setBounds(328, 224, 106, 31);
		contentPane.add(btnCancel);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBackground(Color.GREEN);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				C_ID = Integer.parseInt(cmbcus.getSelectedItem().toString().substring(0, 2).trim());
				delete(C_ID);
			}
		});
		btnDelete.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnDelete.setBounds(63, 224, 117, 31);
		contentPane.add(btnDelete);
		
		

	}

	@SuppressWarnings("deprecation")
	public void delete(int C_ID) {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			stmt = cn.createStatement();
			System.out.println("DELETE from customer where C_ID = " + C_ID);
			boolean i = stmt.execute("DELETE from customer where C_ID = " + C_ID);
			if (i == true) {
				JOptionPane.showMessageDialog(null, "Customer Not Deleted sucessfully", "Error",
						JOptionPane.ERROR_MESSAGE);
				show(false);
				DeleteCustomer frame = new DeleteCustomer();
				frame.setVisible(true);
				show(false);

			} else
			{
				JOptionPane.showMessageDialog(null, "Customer  Deleted Successfully", "Welcome",
						JOptionPane.PLAIN_MESSAGE);
			show(false);
			AgentHome frame = new AgentHome();
			frame.setVisible(true);
			show(false);
			}

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
