package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class Tickets extends JFrame {

	private JPanel contentPane;
	@SuppressWarnings("unused")
	private JTextField txttktno;
	private JTextField txtseatfrom;
	private JTextField txtseatTo;
	private JTextField txtTktcost;
	private JTextField txtTotalSeats;
	private JTextField txtTotalCost;
	String T_NO, busid, C_ID, SEAT_NO_FROM, SEAT_NO_TO, TOT_SEATS, TKT_COST, C_NAME, TotalTktCost;
	Date DOJ;
	int total;
	protected JLabel field;
	private JTextField txtbustype;
	private JTextField txtsrc;
	private JTextField txtdest;
	private JTextField txtdepar;
	private JTextField txtarvl;
	private JTextField txtcusname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		Tickets frame = new Tickets();
		frame.setTitle("Ticket Bookingr");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Tickets() {
		total = 0;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 430);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblTicketDetails = new JLabel("Ticket Details");
		lblTicketDetails.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblTicketDetails.setBounds(271, 22, 137, 21);
		contentPane.add(lblTicketDetails);

		

		JLabel lblBusId = new JLabel("Bus ID");
		lblBusId.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblBusId.setBounds(30, 67, 57, 14);
		contentPane.add(lblBusId);

		JComboBox cmbbusid = new JComboBox();

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from bus ";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbbusid.addItem(rs.getInt(1));

			}
		} catch (Exception e) {

		}

		cmbbusid.setBounds(114, 64, 100, 20);
		contentPane.add(cmbbusid);

		JLabel lblCusId = new JLabel("Cus ID");
		lblCusId.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblCusId.setBounds(30, 106, 46, 14);
		contentPane.add(lblCusId);

		JComboBox cmbcusid = new JComboBox();
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			String str = "Select * from customer ";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);

			while (rs.next()) {
				cmbcusid.addItem(rs.getInt(1));

			}
		} catch (Exception e) {

		}

		cmbcusid.setBounds(114, 103, 100, 20);
		contentPane.add(cmbcusid);

		JLabel lblJrnyDate = new JLabel("Jrny Date");
		lblJrnyDate.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblJrnyDate.setBounds(300, 106, 73, 14);
		contentPane.add(lblJrnyDate);

		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(393, 103, 113, 20);
		contentPane.add(dateChooser);

		JLabel lblSeatFrom = new JLabel("Seat From");
		lblSeatFrom.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblSeatFrom.setBounds(30, 215, 62, 14);
		contentPane.add(lblSeatFrom);

		txtseatfrom = new JTextField();
		txtseatfrom.setBounds(114, 209, 100, 20);
		contentPane.add(txtseatfrom);
		txtseatfrom.setColumns(10);

		JLabel lblSeatTo = new JLabel("Seat To");
		lblSeatTo.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblSeatTo.setBounds(300, 215, 57, 14);
		contentPane.add(lblSeatTo);

		txtseatTo = new JTextField();
		txtseatTo.setBounds(393, 209, 113, 20);
		contentPane.add(txtseatTo);
		txtseatTo.setColumns(10);

		JLabel lblTotalSeats = new JLabel("Tkt Cost");
		lblTotalSeats.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblTotalSeats.setBounds(30, 252, 73, 17);
		contentPane.add(lblTotalSeats);

		txtTktcost = new JTextField();
		txtTktcost.setBounds(114, 250, 100, 20);
		contentPane.add(txtTktcost);
		txtTktcost.setColumns(10);

		JLabel lblTotalSeats_1 = new JLabel("Total Seats");
		lblTotalSeats_1.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblTotalSeats_1.setBounds(300, 253, 73, 14);
		contentPane.add(lblTotalSeats_1);

		txtTotalSeats = new JTextField();
		txtTotalSeats.setBounds(393, 247, 113, 20);
		contentPane.add(txtTotalSeats);
		txtTotalSeats.setColumns(10);

		JLabel lblTotalTktCost = new JLabel("Total Tkt Cost");
		lblTotalTktCost.setFont(new Font("Sitka Subheading", Font.BOLD, 14));
		lblTotalTktCost.setBounds(300, 315, 111, 14);
		contentPane.add(lblTotalTktCost);

		txtTotalCost = new JTextField();
		txtTotalCost.setBounds(420, 309, 105, 20);
		contentPane.add(txtTotalCost);
		txtTotalCost.setColumns(10);

		JButton btnBookNow = new JButton("Book Now");
		btnBookNow.setBackground(Color.GREEN);
		btnBookNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// T_NO=txttktno.getText();
				busid = cmbbusid.getSelectedItem().toString();
				C_ID = cmbcusid.getSelectedItem().toString();
				DOJ = dateChooser.getDate();
				SEAT_NO_FROM = txtseatfrom.getText();
				SEAT_NO_TO = txtseatTo.getText();
				TOT_SEATS = txtTotalSeats.getText();
				TKT_COST = txtTktcost.getText();
				TotalTktCost = txtTotalCost.getText();
				BookNow();
			}
		});
		btnBookNow.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBookNow.setBounds(201, 345, 137, 35);
		contentPane.add(btnBookNow);

		JLabel lblBusType = new JLabel("Bus Type");
		lblBusType.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblBusType.setBounds(300, 64, 57, 21);
		contentPane.add(lblBusType);

		JLabel lblSource = new JLabel("Source");
		lblSource.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblSource.setBounds(30, 139, 57, 21);
		contentPane.add(lblSource);

		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDestination.setBounds(300, 142, 73, 14);
		contentPane.add(lblDestination);

		JLabel lblDepartureTime = new JLabel("Departure Time");
		lblDepartureTime.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblDepartureTime.setBounds(10, 171, 100, 21);
		contentPane.add(lblDepartureTime);

		JLabel lblArrivalTime = new JLabel("Arrival Time");
		lblArrivalTime.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblArrivalTime.setBounds(297, 174, 82, 21);
		contentPane.add(lblArrivalTime);

		txtbustype = new JTextField();
		txtbustype.setBounds(395, 64, 111, 20);
		contentPane.add(txtbustype);
		txtbustype.setColumns(10);

		txtsrc = new JTextField();
		txtsrc.setBounds(114, 134, 100, 20);
		contentPane.add(txtsrc);
		txtsrc.setColumns(10);

		txtdest = new JTextField();
		txtdest.setBounds(393, 139, 113, 20);
		contentPane.add(txtdest);
		txtdest.setColumns(10);

		txtdepar = new JTextField();
		txtdepar.setBounds(120, 171, 94, 20);
		contentPane.add(txtdepar);
		txtdepar.setColumns(10);

		txtarvl = new JTextField();
		txtarvl.setBounds(393, 178, 113, 20);
		contentPane.add(txtarvl);
		txtarvl.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.setBackground(Color.ORANGE);
		btnAdd.setFont(new Font("Sitka Subheading", Font.BOLD, 16));
		btnAdd.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
				try {

					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					int Totaltkt = Integer.parseInt(txtTktcost.getText().trim())
							* Integer.parseInt(txtTotalSeats.getText().trim());
					total = total + Totaltkt;
					txtTotalCost.setText(total + " ");
				} catch (ClassNotFoundException e) {

				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		});
		btnAdd.setBounds(176, 311, 89, 23);
		contentPane.add(btnAdd);

		JLabel lblCusName = new JLabel("Cus Name");
		lblCusName.setFont(new Font("Sitka Subheading", Font.BOLD, 12));
		lblCusName.setBounds(21, 286, 66, 14);
		contentPane.add(lblCusName);

		txtcusname = new JTextField();
		txtcusname.setBounds(114, 281, 100, 20);
		contentPane.add(txtcusname);
		txtcusname.setColumns(10);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBackground(Color.GREEN);
		btnCancel.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnCancel.setBounds(393, 345, 113, 35);
		contentPane.add(btnCancel);

		ItemListener itemListener = new ItemListener() {
			@SuppressWarnings("unused")
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				System.out.println("Bus: " + itemEvent.getItem()); // --
				int busid = (int) itemEvent.getItem();
				try {

					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str1 = " select BUS_TYPE ,TKT_COST, SOURCE,DESTINATION,DEPARTURE_TIME,ARRIVAL_TIME from bus  where bus_id = "
							+ busid;
					Statement st = cn.createStatement();
					System.out.println(str1);
					ResultSet rs = st.executeQuery(str1);

					while (rs.next()) {

						txtbustype.setText(rs.getString(1));
						txtTktcost.setText(rs.getString(2));
						txtsrc.setText(rs.getString(3));
						txtdest.setText(rs.getString(4));
						txtdepar.setText(rs.getString(5));
						txtarvl.setText(rs.getString(6));
					}
				} catch (Exception e) {

				}

			}
		};
		cmbbusid.addItemListener(itemListener);

		ItemListener itemListener1 = new ItemListener() {
			@SuppressWarnings("unused")
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				System.out.println("customer: " + itemEvent.getItem()); // --
				int cid = (int) itemEvent.getItem();
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str2 = " select C_Name from customer where c_id = " + cid;
					Statement st = cn.createStatement();
					// System.out.println(str2);
					ResultSet rs = st.executeQuery(str2);
					while (rs.next()) {
						txtcusname.setText(rs.getString(1));
					}
				} catch (Exception e) {

				}

			}
		};
		cmbcusid.addItemListener(itemListener1);
	}

	@SuppressWarnings("deprecation")
	public void BookNow() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			PreparedStatement st = cn.prepareStatement("insert into tickets values (TktSeq.nextval,?,?,?,?,?,?)");
			st.setString(1, C_ID);
			st.setString(2, SEAT_NO_FROM);
			st.setString(3, SEAT_NO_TO);
			st.setString(4, TOT_SEATS);
			System.out.println(TOT_SEATS);
			java.sql.Date sqlDate = new java.sql.Date(DOJ.getTime());
			st.setDate(5, sqlDate);
			st.setString(6, busid);

			int i = st.executeUpdate();

			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Ticket Booked sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
				show(false);
				TicketView frame = new TicketView();
				frame.setVisible(true);
				show(false);
			} else
				JOptionPane.showMessageDialog(null, "Ticket Not Booked Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);
			// close

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
