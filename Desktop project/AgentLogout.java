package BusReservation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class AgentLogout extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		AgentLogout frame = new AgentLogout();
		frame.setTitle("Agent Logout");
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public AgentLogout() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblExit = new JLabel("Exit");
		lblExit.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblExit.setBounds(189, 23, 73, 32);
		contentPane.add(lblExit);

		JButton btnLogout = new JButton("Logout");
		btnLogout.setBackground(Color.GREEN);
		btnLogout.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit ?", "Conform !",
						JOptionPane.YES_NO_OPTION);
				if (x == JOptionPane.YES_OPTION) {
					setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					 LoginPage frame = new LoginPage();
				     frame.setVisible(true);
				    show(false);
				} else {
					setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					AgentHome frame = new AgentHome();
					frame.setVisible(true);
                    show(false);
					
				}
				
			}
		});

		btnLogout.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnLogout.setBounds(157, 136, 117, 42);
		contentPane.add(btnLogout);
	}

}
