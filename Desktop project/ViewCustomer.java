package BusReservation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;
import java.sql.*;

@SuppressWarnings("serial")
public class ViewCustomer extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	@SuppressWarnings("unused")
	private Container contentPane;
	JTextField txtSear = new JTextField();

	public ViewCustomer() {
		getContentPane().setBackground(new Color(102, 204, 255));
		cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
		model.addColumn("C_ID");
		model.addColumn("C_NAME");
		model.addColumn("PH_NO");

		JButton btnBack = new JButton("Back");
		btnBack.setBackground(Color.GREEN);
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				AgentHome frame = new AgentHome();
				frame.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBack.setBounds(335, 194, 89, 22);
		cnt.add(btnBack);

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
			PreparedStatement pstm = cn.prepareStatement("SELECT * from customer order by C_ID ");
			ResultSet Rs = pstm.executeQuery();
			while (Rs.next()) {
				model.addRow(new Object[] { Rs.getInt(1), Rs.getString(2), Rs.getLong(3) });
			}
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(Color.GREEN);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					System.out.println("in search");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "bus", "bus");
					String str = txtSear.getText().trim();
					System.out.println(str);
					java.sql.Statement st = cn.createStatement();
					String query = "select * from customer where C_NAME like '%" + str + "%'";
					System.out.println(query);
					ResultSet Rs = st.executeQuery(query);
					while (Rs.next() == true) {

						System.out.println(Rs.getString(2));
						model.setRowCount(0);
						model.addRow(new Object[] { Rs.getInt(1), Rs.getString(2), Rs.getLong(3) });
						
					}

				} catch (Exception e) {

				}

			}
		});
		JLabel lblcusname = new JLabel("Enter Customer Name");
		lblcusname.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		getContentPane().add(lblcusname);

		getContentPane().add(txtSear);
		txtSear.setColumns(10);
		btnSearch.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		getContentPane().add(btnSearch);

		JScrollPane pg = new JScrollPane(jtbl);
		cnt.add(pg);
		this.pack();
	}

	public static void main(String[] args) {
		JFrame fr = new ViewCustomer();
		fr.setTitle(" View Customer Details");
		fr.setSize(500, 500);
		fr.setLocationRelativeTo(null);
		fr.setVisible(true);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
